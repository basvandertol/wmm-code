import numpy as np


def solver(A, B, a, suppression):
    """
    A : noise correlation matrix
    B : interference correlation matrix
    """

    N = A.shape[0]

    alpha0 = 0.0
    alpha1 = 1.0

    w0 = mvdr_weights(B, a)
    w1 = mvdr_weights(A, a)

    interference0 = np.dot(np.dot(w0.T.conj(), B), w0)[0,0].real
    interference1 = np.dot(np.dot(w1.T.conj(), B), w1)[0,0].real

    interference_target = suppression * np.trace(B.real)/(N*N)

    while abs(alpha1 - alpha0) > 1e-6:
        print(alpha1, alpha0, interference0, interference1, interference_target)

        alpha = alpha0 + (alpha1 - alpha0)/(interference1 - interference0) * (interference_target - interference0)
        alpha = (alpha0 + alpha1)/2
        print(alpha)

        R = alpha*A + (1-alpha)*B
        w = mvdr_weights(R, a)
        interference = np.dot(np.dot(w.T.conj(), B), w)[0,0].real
        if interference < interference_target :
            interference0 = interference
            alpha0 = alpha
        else:
            interference1 = interference
            alpha1 = alpha

    #while abs(alpha1 - alpha0) > 1e-10:

        #print(alpha1, alpha0, interference0, interference1, interference_target)

        #alpha = alpha0 + (alpha1 - alpha0)/(interference1 - interference0) * (interference_target - interference0)

        #R = alpha*A + (1-alpha)*B
        #w = mvdr_weights(R, a)
        #interference = np.dot(np.dot(w.T.conj(), B), w)[0,0].real

        #interference0 = interference1
        #alpha0 = alpha1
        #interference1 = interference
        #alpha1 = alpha



def mvdr_weights(R, a):

    Rinv = np.linalg.pinv(R)
    w = np.dot(Rinv, a)
    w /= np.dot(w.T.conj(), a)

    return w


N = 48

a = np.ones((N,1), dtype = np.complex128)
A = np.eye(N, dtype = np.complex128)

b = np.random.standard_normal((N,1)) + 1j*np.random.standard_normal((N,1))
B = np.dot(b, b.T.conj())

suppression = .1

solver(A,B,a,suppression)


