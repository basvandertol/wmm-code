#!/usr/bin/env python

from __future__ import print_function

from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt
import numpy as np

import csv

wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions).T



#msname="/home/vdtol/data/imagtest96.MS"
msname="/home/vdtol/data/imagtest.MS"

sr = StationResponse(msname=msname, inverse=False, useElementResponse=True, useArrayFactor=True, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_field = pt.table(ms.getkeyword('FIELD'))
ms_ant = pt.table(ms.getkeyword('ANTENNA'))


ra, dec = ms_field[0]['PHASE_DIR'][0]


tt = pt.taql("SELECT UNIQUE TIME FROM $ms")
times = tt.getcol('TIME')

freqtable = pt.table(ms.getkeyword('SPECTRAL_WINDOW'))
freqs = freqtable[0]['CHAN_FREQ']

time = times[len(times)//2]

sr.setDirection(ra+np.pi/2, 0) # default is phase center
itrf_ra_dir = sr.getDirection(time)

sr.setDirection(ra, dec + np.pi/2) # default is phase center
itrf_dec_dir = sr.getDirection(time)

sr.setDirection(ra, dec) # default is phase center

itrfdir = sr.getDirection(time)
refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)


station = 17

freq = freqs[0]

N = 101
r = np.zeros((N,N))


N = 101
r0 = np.zeros((N,N))
r = np.zeros((N,N))

#for i,l in enumerate(np.linspace(-0.2, 0.2, N)):
    #for j, m in enumerate(np.linspace(-0.2, 0.2, N)):
        #d = itrfdir + l * itrf_ra_dir + m * itrf_dec_dir
        #d /= np.linalg.norm(d)

        ## bj is a 2x2 jones matrix with the modeled beam response
        #bj = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

        #r0[i,j] = abs(bj[0,0])**2

        #r[i,j] = abs(sr.evaluateBeamFormer(station, d)[0,0])**2


#clf()

#subplot(1,2,1)
#imshow(10*log(r0), interpolation = 'nearest', clim = (-100,0))
#colorbar()

#subplot(1,2,2)
#imshow(10*log(r), interpolation = 'nearest', clim = (-100,0))
#colorbar()

station_pos = ms_ant[station]["POSITION"]

#v = station_pos.copy()

#v /= np.linalg.norm(v)

#v1 = array([v[1], -v[0], 0])
#v1 /= np.linalg.norm(v1)
#v2 = array([-v[0], -v[1], (v[0]*v[0]+v[1]*v[1])/v[2]])
#v2 /= np.linalg.norm(v2)




#N = 501
#r0 = np.zeros((N,N))
#r = np.zeros((N,N))

#for i,l in enumerate(np.linspace(-1.0, 1.0, N)):
    #for j, m in enumerate(np.linspace(-1.0, 1.0, N)):
        #n = np.sqrt(1 - l**2 - m**2)
        #if not np.isnan(n):
            #theta = np.sqrt(l**2 + m**2) * np.pi/2
            #phi = np.arctan2(m,l)

            #m1 = sin(phi) * sin(theta)
            #l1 = cos(phi) * sin(theta)
            #n1 = cos(theta)

            #d = l1 * v1 + m1 * v2 + n1 * v
            #d /= np.linalg.norm(d)

            ## bj is a 2x2 jones matrix with the modeled beam response
            #bj = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

            #r0[j,i] = sum(abs(bj)**2)

            #r[j,i] = abs(sr.evaluateBeamFormer(station, d)[0,0])**2


#figure()

#subplot(1,2,1)
#imshow(10*log(r0), interpolation = 'nearest', clim = (-100,0))
#colorbar()

#subplot(1,2,2)
#imshow(10*log(r), extent = (-1.0, 1.0, -1.0, 1.0), interpolation = 'nearest', clim = (-100,0))
#colorbar()

#d = wm_positions - station_pos[:, np.newaxis]

#d /= np.linalg.norm(d, axis=0)


#l = np.dot(v1, d)
#m = np.dot(v2, d)

#plot(-l, m, 'kx', markeredgewidth=2)

#wm = 31
wm = 9

freq = 70e6

up = wm_positions[:,wm].copy()


up /= np.linalg.norm(up)

los = wm_positions[:,wm] - station_pos
los /= np.linalg.norm(los)


perp = np.cross(up,los)

sr.setBeamFormer(station, itrfdir, frequency=freq, null_positions = wm_positions[:,[wm]]+up[:,np.newaxis]*145)
print(sr.evaluateBeamFormer(station, itrfdir))


extent = np.array([-200,200,0,250])
pixel_size_x = (extent[1] - extent[0])/(N-1)
pixel_size_y = (extent[3] - extent[2])/(N-1)

extent_outer = extent + np.array([-pixel_size_x/2, pixel_size_x/2, -pixel_size_y/2, pixel_size_y/2])

for offset_dir, offset_dir_description, plot_name_suffix in ((los, 'along', 'los'), (perp, 'perpendircular to', 'perp_los')):
    r1 = np.zeros((N,N))
    for i,x in enumerate(np.linspace(extent[0], extent[1], N)):
        for j, y in enumerate(np.linspace(extent[2], extent[3], N)):
                r1[j,i] = 20*log10(abs(sr.evaluateBeamFormer(station, wm_positions[:,wm] + x*offset_dir + y*up, frequency=freq, near_field=True)[0,0]))

    plt.figure()
    plt.imshow(r1,extent=extent_outer, origin='lower', vmin=-60, vmax=-17)
    plt.colorbar()
    clabel(contour(r1, [-90,-80,-70,-60,-50,-40,-35,-30,-25,-20,-15,-10, 0],extent=extent ))


    wm_diameter = 131
    wm_axis_height = 145

    circle = np.exp(1j*np.linspace(0,2*np.pi,100))*wm_diameter/2 + 1j*wm_axis_height
    plt.plot(circle.real, circle.imag, 'k')
    plt.plot((0,0), (0, wm_axis_height), 'k')
    plt.plot(0, wm_axis_height, 'k.')

    plt.gca().set_aspect('equal')

    plt.xlabel('Offset {} line-of-sight (m)'.format(offset_dir_description))
    #plt.xlabel('Offset perpendicular to line-of-sight (m)')
    plt.ylabel('Height (m)')

    plt.show()
    plt.savefig('beamplot_{}.pdf'.format(plot_name_suffix))





