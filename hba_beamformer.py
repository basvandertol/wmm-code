import casacore.tables
import numpy as np
from matplotlib import pyplot as plt
import csv
import scipy.constants
import scipy.optimize



t = casacore.tables.table('/home/vdtol/data/HBA-test.MS/')

t_af = casacore.tables.table(t.getkeyword('LOFAR_ANTENNA_FIELD'))

station = 4

af_name = t_af[station]['NAME']

station_position = t_af[station]['POSITION']

tile_offsets = t_af[station]['ELEMENT_OFFSET']

tile_flags = t_af[station]['ELEMENT_FLAG']

if af_name == 'HBA0':
    tile_offsets = tile_offsets[:24,:]
else:
    tile_offsets = tile_offsets[24:,:]

#tile_offsets = tile_offsets[tile_flags[:,0],:]

#select only one tile
#tile_offsets = tile_offsets[[0], :]

N_tiles = tile_offsets.shape[0]

coordinate_axes = t_af[station]['COORDINATE_AXES']

element_offsets = t_af[station]['TILE_ELEMENT_OFFSET']

#select one element only
#element_offsets = element_offsets[:1,:]

N_elements_per_tile = element_offsets.shape[0]

p = np.dot((tile_offsets[:,None,:] + element_offsets[None,:,:]).reshape((-1,3)), coordinate_axes.T)

element_positions = (station_position[None,None,:] + tile_offsets[:,None,:] + element_offsets[None,:,:]).reshape((-1,3))

wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions)

N_wm = wm_positions.shape[0]

d = np.sqrt(np.sum((element_positions[:, None, :] - wm_positions[None, :, :]) ** 2, axis=-1))

frequency = 140e6

wavelength =  scipy.constants.speed_of_light / frequency

Ai = 1/d**4 * np.exp(-2* np.pi * 1j * d/wavelength)

Ri = 1e30 * np.dot(Ai, Ai.T.conj())




def cost(x, R, a):
    phases = x

    w = np.exp(1j*phases)
    return np.dot(w.T.conj(), np.dot(R, w)).real / np.abs(np.dot(w.T.conj(), a))**2


N = N_tiles * N_elements_per_tile

a = np.ones((N,1))

phases0 = np.angle(a)

for alpha in np.linspace(-2,0,50):

    R = np.eye(N) + 10.0**alpha * Ri

    res = scipy.optimize.minimize(cost, phases0, (R, a), tol=1e-7)

    phases = res.x

    w = np.exp(1j*phases)



    noise_power = N**2
    signal_power = np.abs(np.sum(w))**2
    interference_power = np.abs(np.dot(np.dot(w.T.conj(), Ri), w))


    SNR = signal_power / noise_power

    plt.plot(signal_power/noise_power, 10*log10(interference_power),'x')
    plt.show()
    pause(0.001)

plt.show()

#plt.plot(p[:,0], p[:,1], '.')
#ax = gca()
#ax.set_aspect('equal')

#plt.show()

