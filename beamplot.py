#!/usr/bin/env python

from __future__ import print_function

from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt
import numpy as np



wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions).T


msname="/home/vdtol/data/imagtest.MS"


sr = StationResponse(msname=msname, inverse=False, useElementResponse=False, useArrayFactor=True, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_field = pt.table(ms.getkeyword('FIELD'))

ra, dec = ms_field[0]['PHASE_DIR'][0]


tt = pt.taql("SELECT UNIQUE TIME FROM $ms")

times = tt.getcol('TIME')

freqtable = pt.table(msname+'::SPECTRAL_WINDOW')
freqs = freqtable[0]['CHAN_FREQ']


for time in times[::100]:

    sr.setDirection(ra+np.pi/2, 0) # default is phase center
    itrf_ra_dir = sr.getDirection(time)

    sr.setDirection(ra, dec + np.pi/2) # default is phase center
    itrf_dec_dir = sr.getDirection(time)

    sr.setDirection(ra, dec) # default is phase center

    itrfdir = sr.getDirection(time)
    refdelay = sr.getRefDelay(time)
    reftile = sr.getRefTile(time)


    station = 0

    freq = freqs[0]

    sr.setBeamFormer(station, itrfdir, [])

    r = sr.getResponseVector(station, itrfdir)

    W = np.zeros((len(r),30), dtype = np.complex128)
    for i in range(W.shape[1]):
        W[:,[i]] = sr.getResponseVector(station, wm_positions[:,[i]], near_field=True)

    P = np.eye(len(W)) - np.dot(np.dot(W, np.linalg.inv(np.dot(W.T.conj(), W))), W.T.conj())
    a = np.dot(P,r)
    a /= np.dot(a.T.conj(), r)
    sr.setBeamFormerWeights(station, a)

    N = 101
    r0 = np.zeros((N,N))
    r = np.zeros((N,N))

    for i,l in enumerate(np.linspace(-0.3, 0.3, N)):
        for j, m in enumerate(np.linspace(-0.3, 0.3, N)):
            d = itrfdir + l * itrf_ra_dir + m * itrf_dec_dir
            d /= np.linalg.norm(d)

            # bj is a 2x2 jones matrix with the modeled beam response
            bj = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

            r0[i,j] = abs(bj[0,0])**2

            r[i,j] = abs(sr.evaluateBeamFormer(station, d)[0,0])**2

    clf()

    subplot(1,2,1)
    imshow(10*log(r0), interpolation = 'nearest', clim = (-100,0))
    colorbar()

    subplot(1,2,2)
    imshow(10*log(r), interpolation = 'nearest', clim = (-100,0))
    colorbar()

    pause(0.1)
