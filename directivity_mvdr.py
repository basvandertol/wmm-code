from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt
import csv
from matplotlib import pyplot as plt
import numpy as np


wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions).T



msname="/home/vdtol/data/imagtest96.MS"

sr = StationResponse(msname=msname, inverse=False, useElementResponse=True, useArrayFactor=False, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_ant = pt.table(ms.getkeyword('ANTENNA'))
ms_freq = pt.table(ms.getkeyword('SPECTRAL_WINDOW'))
freqs = ms_freq[0]['CHAN_FREQ']

tt = pt.taql("SELECT UNIQUE TIME FROM $ms")
times = tt.getcol('TIME')


station = 0
station_pos = ms_ant[station]["POSITION"]

v = station_pos.copy()

v /= np.linalg.norm(v)

v1 = np.array([v[1], -v[0], 0])
v1 /= np.linalg.norm(v1)
v2 = np.array([-v[0], -v[1], (v[0]*v[0]+v[1]*v[1])/v[2]])
v2 /= np.linalg.norm(v2)

N = 100


dtheta = np.pi/2/N
dphi = 2*np.pi/N

theta_range = np.linspace(0.0, np.pi/2, N+1)
theta_range = (theta_range[:-1] + theta_range[1:])/2

phi_range = np.linspace(-np.pi, np.pi, N+1)
phi_range = (phi_range[:-1] + phi_range[1:])/2

Aeffs = []

time = times[len(times)//2]
freq = freqs[0]

refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)

pol = 0

itrfdir = sr.getDirection(time)
refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)

sr.setBeamFormer(station, itrfdir, freq, [])

r = sr.getResponseVector(station, itrfdir)

nr_antennas = len(r)

W = np.zeros((nr_antennas,wm_positions.shape[1]), dtype = np.complex128)
for i in range(W.shape[1]):
    W[:,[i]] = sr.getResponseVector(station, wm_positions[:,[i]], near_field=True)

distance = np.sqrt(np.sum((wm_positions - station_pos[:,np.newaxis])**2, axis=0))

S = np.diag(1.0/distance**4)


R0 = np.dot(np.dot(W, S), W.T.conj())

a = sr.getResponseVector(station, itrfdir)

w = a.copy()
w /= np.dot(np.conj(w.T), a)

p0 = np.dot(np.dot(w.T.conj(), R0), w)[0,0].real


#clf()

#for i in np.linspace(0,15, 250):
for i in np.linspace(11,15, 50):
#for i in np.linspace(4,7, 100):

    R = np.dot(np.dot(W, (10.0**i)*S), W.T.conj()) + np.eye(nr_antennas)
    Rinv = np.linalg.inv(R)


    w = np.dot(Rinv, a)

    w /= np.dot(np.conj(w.T), a)

    sr.setBeamFormerWeights(station, np.conj(w))

    r = 0.0
    max_gain = 0.0

    for theta in theta_range:
        for phi in phi_range:
            m = np.sin(phi) * np.sin(theta)
            l = np.cos(phi) * np.sin(theta)
            n = np.cos(theta)

            d = l * v1 + m * v2 + n * v

            bj0 = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

            #bj = np.eye(2, dtype=np.complex128)

            bj1 = sr.evaluateBeamFormer(station, d)


            bj = np.dot(bj1, bj0)

            gain = (abs(bj[pol,0])**2 + abs(bj[pol,1])**2)

            #rv = sr.getResponseVector(station, d)

            #gain = (abs(np.dot(conj(a.T), rv))[0,0])**2
            #bj *= np.dot(conj(a.T), rv)[0,0]
            #gain = (abs(bj[pol,0])**2 + abs(bj[pol,1])**2)


            r += dtheta*dphi*np.sin(theta) * gain
            max_gain = max(max_gain, gain)



    directivity = 4*np.pi*max_gain/r

    directivity1 = 1/np.sum(abs(w)**2)

    #directivity = 4*np.pi*1.0/r

    p = np.dot(np.dot(w.T.conj(), R0), w)[0,0].real


    print(i, directivity, max_gain, 10*np.log10(p/p0))
    plt.plot(directivity, 10*np.log10(p/p0), 'b.')
    plt.plot(directivity1/96*310, 10*np.log10(p/p0), 'bx')
    plt.xlabel('Directivity')
    plt.ylabel('Nulling depth (dB)')
    plt.pause(0.001)



Aeff_per_lambda_squared = max_gain/r


speed_of_light = 299792458.0

wavelength = speed_of_light / freqs[0]

Aeff = Aeff_per_lambda_squared * wavelength**2

print(Aeff)

