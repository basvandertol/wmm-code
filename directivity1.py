from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt
import csv



wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions).T



msname="/home/vdtol/data/imagtest96.MS"

sr = StationResponse(msname=msname, inverse=False, useElementResponse=True, useArrayFactor=False, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_ant = pt.table(ms.getkeyword('ANTENNA'))
ms_freq = pt.table(ms.getkeyword('SPECTRAL_WINDOW'))
freqs = ms_freq[0]['CHAN_FREQ']

tt = pt.taql("SELECT UNIQUE TIME FROM $ms")
times = tt.getcol('TIME')


station = 0
station_pos = ms_ant[station]["POSITION"]

v = station_pos.copy()

v /= np.linalg.norm(v)

v1 = array([v[1], -v[0], 0])
v1 /= np.linalg.norm(v1)
v2 = array([-v[0], -v[1], (v[0]*v[0]+v[1]*v[1])/v[2]])
v2 /= np.linalg.norm(v2)



N = 100


dtheta = np.pi/2/N
dphi = 2*np.pi/N

theta_range = np.linspace(0.0, np.pi/2, N+1)
theta_range = (theta_range[:-1] + theta_range[1:])/2

phi_range = np.linspace(-np.pi, np.pi, N+1)
phi_range = (phi_range[:-1] + phi_range[1:])/2

Aeffs = []

time = times[len(times)/2]
freq = freqs[0]

refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)




pol = 0



itrfdir = sr.getDirection(time)
refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)

sr.setBeamFormer(station, itrfdir, [])

r = sr.getResponseVector(station, itrfdir)

W = np.zeros((len(r),wm_positions.shape[1]), dtype = np.complex128)
for i in range(W.shape[1]):
    W[:,[i]] = sr.getResponseVector(station, wm_positions[:,[i]], near_field=True)

for i in range(0,W.shape[1]):

    r = sr.getResponseVector(station, itrfdir)

    a = r.copy()

    if i:
        W1 = W[:,-i:]
        P = np.eye(len(W1)) - np.dot(np.dot(W1, np.linalg.inv(np.dot(W1.T.conj(), W1))), W1.T.conj())
        a = np.dot(P,a)

    a /= np.dot(conj(a.T), r)

    sr.setBeamFormerWeights(station, conj(a))

    r = 0.0
    max_gain = 0.0

    for theta in theta_range:
        for phi in phi_range:
            m = sin(phi) * sin(theta)
            l = cos(phi) * sin(theta)
            n = cos(theta)

            d = l * v1 + m * v2 + n * v

            bj = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

            #bj = np.eye(2, dtype=np.complex128)

            #bj = sr.evaluateBeamFormer(station, d)
            #gain = (abs(bj[pol,0])**2 + abs(bj[pol,1])**2)

            rv = sr.getResponseVector(station, d)

            #gain = (abs(np.dot(conj(a.T), rv))[0,0])**2

            bj *= np.dot(conj(a.T), rv)[0,0]

            gain = (abs(bj[pol,0])**2 + abs(bj[pol,1])**2)


            r += dtheta*dphi*np.sin(theta) * gain
            max_gain = max(max_gain, gain)



    directivity = 4*np.pi*max_gain/r

    #directivity = 4*np.pi*1.0/r

    print i, directivity, max_gain


Aeff_per_lambda_squared = max_gain/r


speed_of_light = 299792458.0

wavelength = speed_of_light / freqs[0]

Aeff = Aeff_per_lambda_squared * wavelength**2

print(Aeff)

