import casacore.measures
import casacore.tables

import numpy as np
import csv

def dir_measure_to_xyz(dm):
    lon = dm['m0']['value']
    lat = dm['m1']['value']
    x = np.cos(lat) * np.cos(lon)
    y = np.cos(lat) * np.sin(lon)
    z = np.sin(lat)

    return np.array([x,y,z])

def enu(position):
    """east, north, up coordinates system for position (ITRF)"""

    m = casacore.measures.measures()

    p = m.position()

    m.do_frame(p)

    east = m.direction('AZEL', '90deg', '0deg')
    north = m.direction('AZEL', '0deg', '0deg')
    up = m.direction('AZEL', '0deg', '90deg')

    east_itrf = dir_measure_to_xyz(m.measure(east, 'ITRF'))
    north_itrf = dir_measure_to_xyz(m.measure(north, 'ITRF'))
    up_itrf = dir_measure_to_xyz(m.measure(up, 'ITRF'))


    t_ant = casacore.tables.table('/home/vdtol/data/imagtest96.MS/ANTENNA')
    station_positions = t_ant.getcol('POSITION').T


    T = np.stack((east_itrf, north_itrf, up_itrf))



wm_positions = []
with open('drentse-monden-coordinates-RD-ETRS.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        wm_positions.append([np.float64(x) for x in row[5:8]])

wm_positions = np.array(wm_positions).T


m = casacore.measures.measures()

p = m.observatory('LOFAR')
p['refer'] = 'WGS84'


m.do_frame(p)

east = m.direction('AZEL', '90deg', '0deg')
north = m.direction('AZEL', '0deg', '0deg')
up = m.direction('AZEL', '0deg', '90deg')

east_itrf = dir_measure_to_xyz(m.measure(east, 'ITRF'))
north_itrf = dir_measure_to_xyz(m.measure(north, 'ITRF'))
up_itrf = dir_measure_to_xyz(m.measure(up, 'ITRF'))


t_ant = casacore.tables.table('/home/vdtol/data/imagtest96.MS/ANTENNA')
station_positions = t_ant.getcol('POSITION').T

station_names = t_ant.getcol('NAME')

T = np.stack((east_itrf, north_itrf, up_itrf))

p = np.dot(T, wm_positions - station_positions[:,[0]])
plt.plot(p[0], p[1], 'x')
ax = plt.gca()
ax.set_clip_on(True)



for i, p1 in enumerate(p.T):
    plt.text(p1[0], p1[1], str(i), clip_on=True)

p = np.dot(T, station_positions - station_positions[:,[0]])
plt.plot(p[0], p[1], 'o')
for i, p1 in enumerate(p.T):
    plt.text(p1[0], p1[1], str(i))


ax.set_aspect('equal')
plt.xlabel('West -> East (m)')
plt.ylabel('South -> North (m)')

# find minimum distance between station and windmill
d = np.sqrt(np.sum((wm_positions[:,:,np.newaxis] - station_positions[:, np.newaxis,:])**2, axis=0))
min_d_wm_idx, min_d_station_idx = np.unravel_index(np.argmin(d), d.shape)

closest_wm_pos = wm_positions[:,[min_d_wm_idx]]
closest_station_pos = station_positions[:, [min_d_station_idx]]

closest_wm_pos = np.dot(T, closest_wm_pos - station_positions[:,[0]])[:2]
closest_station_pos = np.dot(T, closest_station_pos - station_positions[:,[0]])[:2]

plt.plot([closest_wm_pos[0,0], closest_station_pos[0,0]], [closest_wm_pos[1,0], closest_station_pos[1,0]], 'k')

ax.annotate("",
            xy=(closest_wm_pos[0,0], closest_wm_pos[1,0]), xycoords='data',
            xytext=(closest_station_pos[0,0], closest_station_pos[1,0]), textcoords='data',
            arrowprops=dict(arrowstyle="<->",))


plt.xlim(-2000, 10000)
plt.ylim(-5000, 6000)

plt.savefig("lofar_wm_map.pdf")

plt.show()


