#!/usr/bin/env python

from __future__ import print_function

from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt
import numpy as np

msname="/home/vdtol/data/imagtest.MS"


sr = StationResponse(msname=msname, inverse=False, useElementResponse=False, useArrayFactor=True, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_field = pt.table(ms.getkeyword('FIELD'))

ra, dec = ms_field[0]['PHASE_DIR'][0]


tt = pt.taql("SELECT UNIQUE TIME FROM $ms")

times = tt.getcol('TIME')

freqtable = pt.table(msname+'::SPECTRAL_WINDOW')
freqs = freqtable[0]['CHAN_FREQ']

time = times[0]

sr.setDirection(ra+np.pi/2, 0) # default is phase center
itrf_ra_dir = sr.getDirection(time)

sr.setDirection(ra, dec + np.pi/2) # default is phase center
itrf_dec_dir = sr.getDirection(time)

sr.setDirection(ra, dec) # default is phase center

itrfdir = sr.getDirection(time)
refdelay = sr.getRefDelay(time)
reftile = sr.getRefTile(time)


station = 0

freq = freqs[0]

N = 101
r = np.zeros((N,N))

sr.setBeamFormer(station, itrfdir, [])

r = sr.evaluateBeamFormer(station, itrfdir)

r0 = sr.evaluateFreqITRF(time, station, freq, itrfdir, refdelay, reftile)


