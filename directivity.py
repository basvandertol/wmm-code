from lofar.stationresponse import stationresponse as StationResponse
import casacore.tables as pt

msname="/home/vdtol/data/imagtest.MS"

sr = StationResponse(msname=msname, inverse=False, useElementResponse=True, useArrayFactor=True, useChanFreq=True)

ms = pt.table(msname, ack=False)
ms_ant = pt.table(ms.getkeyword('ANTENNA'))
ms_freq = pt.table(ms.getkeyword('SPECTRAL_WINDOW'))
freqs = ms_freq[0]['CHAN_FREQ']

tt = pt.taql("SELECT UNIQUE TIME FROM $ms")
times = tt.getcol('TIME')


station = 0
station_pos = ms_ant[station]["POSITION"]

v = station_pos.copy()

v /= np.linalg.norm(v)

v1 = array([v[1], -v[0], 0])
v1 /= np.linalg.norm(v1)
v2 = array([-v[0], -v[1], (v[0]*v[0]+v[1]*v[1])/v[2]])
v2 /= np.linalg.norm(v2)



N = 50


dtheta = np.pi/2/N
dphi = 2*np.pi/N

theta_range = np.linspace(0.0, np.pi/2, N+1)
theta_range = (theta_range[:-1] + theta_range[1:])/2

phi_range = np.linspace(-np.pi, np.pi, N+1)
phi_range = (phi_range[:-1] + phi_range[1:])/2

Aeffs = []

for time in times[::10]:

    refdelay = sr.getRefDelay(time)
    reftile = sr.getRefTile(time)

    r = 0.0

    max_gain = 0.0

    pol = 0

    for theta in theta_range:
        for phi in phi_range:
            m = sin(phi) * sin(theta)
            l = cos(phi) * sin(theta)
            n = cos(theta)

            d = l * v1 + m * v2 + n * v

            bj = sr.evaluateFreqITRF(time, station, freq, d, refdelay, reftile)

            gain = (abs(bj[pol,0])**2 + abs(bj[pol,1])**2)

            r += dtheta*dphi*np.sin(theta) * gain
            max_gain = max(max_gain, gain)


    directivity = 4*np.pi*max_gain/r

    Aeff_per_lambda_squared = max_gain/r


    speed_of_light = 299792458.0

    wavelength = speed_of_light / freqs[0]

    Aeff = Aeff_per_lambda_squared * wavelength**2

    print(Aeff)

    Aeffs.append(Aeff)

    clf()
    plot(Aeffs)
    pause(1e-6)


